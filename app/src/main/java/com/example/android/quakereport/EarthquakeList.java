package com.example.android.quakereport;

public class EarthquakeList {

    private double mMag;
    private String mLocation, mURL;
    private long mTimeInMilliseconds;

    public EarthquakeList(double mag, String location, long timeInMilliseconds, String url) {
        mMag = mag;
        mLocation = location;
        mTimeInMilliseconds = timeInMilliseconds;
        mURL = url;
    }

    /**
     * Get the magnitude
     */
    public double getMag() {
        return mMag;
    }

    /**
     * Get the location
     */
    public String getLocation() {
        return mLocation;
    }

    /**
     * Get the timestamp
     */
    public long getTimeInMilliseconds() {
        return mTimeInMilliseconds;
    }

    public String getURL() {
        return mURL;
    }
}
